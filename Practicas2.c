#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*
 * Josué López Mendoza
 * 2020032051
 * Tarea 2
 */






//La función recibe un arreglo como parametro de entrada.
//Se establecen tres variables,1-largo(para medir el arreglo),2-cont(para poder recorrer el arreglo),3-TextoNuevo(Para almacenar despues de cambios).
//Cuando se terminan los ciclos se retorna la variable de textoNuevo sin un caracter(C).
char* eliminar(char*texto,char caracter)
{
	int largo,cont=0;
	largo=strlen(texto);
	int cont2=0;
	
	char* textoNuevo= calloc(largo,sizeof(char));

	while(texto[cont]!='\0')
	{
		
		if(texto[cont]!=caracter)
		{
			textoNuevo[cont2]=texto[cont];
			cont2++;
			
		}
		cont++;
		
	}
	return(textoNuevo);
}
//La función recibe un arreglo como parametro de entrada.
//Se establecen tres variables,1-largo(para medir el arreglo),2-cont(para poder recorrer el arreglo),3-TextoNuevo(Para almacenar despues de cambios).
//Cuando se terminan los ciclos se retorna la variable de textoNuevo con el texto invertido.
char* invertir(char*texto)
{
	int largo,cont=0;
	largo=strlen(texto);
	char* textoNuevo=calloc(largo,sizeof(char));
	
	for(int k=largo-1;k>=0;k--)
	{
		textoNuevo[cont]=texto[k];
		cont++;
	}
		
	return(textoNuevo);
}
//La función recibe un arreglo como parametro de entrada.
//Se establecen dos variables,1-largo(para medir el arreglo),2-TextoNuevo(Para almacenar despues de cambios).
//Cuando se terminan los ciclos se retorna la variable de textoNuevo con el texto litaral en alamacenado en el heap.		
char* LiteralToHeap(char*textoo)
{
	int largo=0;
	largo=strlen(textoo);
	char* textoNuevo=calloc(largo,sizeof(char));
	for(int i =0;i<=largo-1;i++)
	{
		textoNuevo[i]=textoo[i];
	}
	return(textoNuevo);
}
	
/*
* Se inicializa una variables para la opción a elegir.
* Las funciones se van a llamar estableciendo una varible ( int resultados = nombre de la función(paramtro de entrada)).
* Cuando la opereción ejecutada termine se retornara el main().
*/	
int main()
{
	int resultado=0;
	printf("\nSe le presentaran diferentes funciones\n");
	printf("1-Eliminar un caracter de un arreglo\n");
	printf("2-Invertir un texto\n");
	printf("3-Pasar un texto literal a un texto heap\n");
	printf("Opción:");
	scanf("%d",&resultado);
	if(resultado==3)
	{
		char* texto="Pedrito";
		printf("Texto a evaluar:");
		puts(texto);
		char* retorno=LiteralToHeap(texto);
		printf("Realizado con exito:");
		puts(retorno);
		return main();
	}
	else if(resultado==1)
	{
		char* texto="Geclcaticnacc";
		char caracter='c';
		printf("Texto a evaluar:");
		puts(texto);
		char* retorno=eliminar(texto,caracter);
		printf("Realizado con exito:");
		puts(retorno);
		return main();
	}
	else if(resultado==2)
	{
		char* texto="Carro feo";
		printf("Texto a evaluar:");
		puts(texto);
		char* retorno=invertir(texto);
		printf("Realizado con exito:");
		puts(retorno);
		return main();
	}
	else
	{
		printf("\nOpción invalida\n");
		return main();
	}

	
}
	
	
	
	
	
	
	

