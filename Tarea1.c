#include <stdio.h>
/*
 * Josué López Mendoza
 * 2020032051
 * Tarea 1
 */



/* La función recibe un parámetro de entrada al cual se le aplicara la formula fibonacci (parámetro de entrada -1 + parámetro de entrada -2  */
/* La función tiene como retricciones: Cuando el parámetro de entrada sea 0 se retornara un 0 y cuando sea 1 se retornara 1   */
/* Cuado la función termine la recursividad se retornara el valor fibonacci  */
int fibo(int dato)
{
	if (dato==0)
	{
		return 0;
	}
	if (dato==1)
	{
		return 1;
	}
	else
	{
		return fibo(dato-1)+fibo(dato-2);
	}
}
/* La función recibe un parámetro de entrada al cual se le aplicara la formula del factorial (parámetro de entrada * parámetro de entrada -1 ) */
/* La retricción de la función es cuando el parámetro de entrada sea 0 el resultado a retornar sera 1 */
/* Cuando la función termine la recursividad se retornara el valor en factorial   */
int fact(int dato)
{
	if (dato==0)
	{
		return 1;
	}
	else
	{
		return dato * fact(dato-1);
	}
}
/* La función tiene un parámetro de entrada al cual se le aplicara una sumatoria la cual consiste en sumar desde 0 hasta el parámetro de entrada, el resultado de la s sumas estara almacenado en una variable*/
/* Cuando la función termine la iteración se retornara el contador que contiene la suma  */
int sumatoria(int dato)

{
	int cont=0;
	for(int j=0;j<=dato;j++)
	{
		cont+=j;
	}
	return cont;
}
/* La función tiene un parámetro de entrada al cual se le aplicara una sumatoria la cual consiste en sumar desde 0 hasta el parámetro de entrada, el resultado de la s sumas estara almacenado en una variable*/
/* Cuando la función termine la iteración se retornara el contador que contiene la suma  */	

int sumawhile(int dato)
{	
	int cont=0;
	int num=0;
	while(num<=dato)
	{
		cont= cont + num;
		num= num + 1;
	}
	return cont;
}
/* La función recibe un parámetro de entrada , se tienen que inicializar tres variables 1-para almacenar el ultimo dato,2-para almacenar número sin su ultimo digito, 3 para ir acomodando el número invertido       */
/* Solo cuando el parámetro de entrada sea difirente a 0 la iteración se va a ejecutar*/
/* Cuando se termine la itereción se retornara el número invertido   */
int invertir(int dato)
{
	int res1,res2,invertido=0;
	while(dato!=0)
	{
		res1=dato%10;
		res2=dato/10;
		invertido= invertido*10+res1;
		dato=res2;
	}
	return invertido;
}
/* La función recibe un parámetro de entrada para verificar si es un palindromo, se tienen que inicializar cuatro variables 1-para almacenar el ultimo dato,2-para almacenar número sin su ultimo digito, 3 para ir acomodando el número invertido
 * 4- una maś para almacenar el parámetro de entrada para posteriormente comparar el número inverido con el parámetrode entrada  */
/* Solo cuando el parámetrode entrada sea difirente a 0 la iteración se va a ejecutar*/
/* Cuando se termine la itereción se retornara un printf  si el número es un palindromo o no */
void palindromo(int dato)
{
	int res1,res2,invertido=0;
	int datotemp=0;
	datotemp=dato;
	while(dato!=0)
	{
		res1=dato%10;
		res2=dato/10;
		invertido= invertido*10+res1;
		dato=res2;
	}
	if (datotemp==invertido)
	{
		printf("El número es palindromo\n");
	}
	else
	{
		printf("El número no es palindromo\n");
	}
}
/*La función recibe un parámetro de entrada entero, luego establecen dos variables_1-Sirve para almacenar la cantidad de números que tiene el parámetro de entrada(cont),
 * 2-Cada que el número se corte inmediatamente el nuevo número se guarda en la variable(NuevoNum) para posteriormente guardar el contenido de esa variable en el parámetro de entrada.
 * Cuando se se termine el ciclo se retornara la variable cont.
 */	
void largoNumeroW(int dato)
{
	
	int cont=1;
	int Nuevonum=0;
	
	while (dato>9)
	{
		Nuevonum=dato/10;
		cont=cont+1;
		dato=Nuevonum;
	}
	printf("El largo del número es: %d\n",cont);
}
/*La función recibe un parámetro de entrada entero, luego establecen dos variables_1-Sirve para almacenar la cantidad de números que tiene el parámetro de entrada(cont)
 * 2- la variable (i) avanza desde 0  y se va sumando al contador.
 * Cuando se se termine el ciclo se retornara la variable cont.
 */	
void largoNumeroF(int dato)
{
	int cont=0;
	
	for(int i=1;dato>0;dato=dato/10)
		{
			cont+=1;
			i++;
		}
	printf("El largo del número es :%d \n",cont);
}
/*Recibe un parámetro de entrada entero.
 * Si el residuo del parámetro de entrada es 0 se hace un printf(Es par) si no un printf(Es impar)
 */
void parImpar(int dato)
{
	
	if (dato%2==0)
	{
		
		printf("Es par\n");
		
	}
	else
	{
		printf("Es impar\n");
		
	}
}
	
	
	
	
	
/*
	 * Se inicializan dos variables una para almacenar el dato a evaluar y la otra para la opción a elegir.
	 * Las funciones se van a llamar estableciendo una varible ( int resultados = nombre de la función(paramtro de entrada)).
	 * Cuando la opereción ejecutada termine se retornara el main().
	 */	
int main(){
	
	
	int dato=0;
	int resultado=0;
	printf("\nSe le mostraran diferentes opereciones\n");
	printf("1-Par e impar\n");
	printf("2-Fibonacci \n");
	printf("3-Factorial\n");
	printf("4-Largo de un número con el ciclo while\n");
	printf("5-Largo de un número con el ciclo for\n");
	printf("6-Sumatoria de 0 a n usando for\n");
	printf("7-Sumatoria de 0 a n usando while\n");
	printf("8-Invertir un número\n");
	printf("9-Palindromo\n");
	printf("Digite la opción:");
	
	scanf("%d",&resultado);
	if (resultado==2)
	{
		printf("Digite el número:");
		scanf("%d",&dato);
		
		int resultados= fibo(dato);
		printf("El resultado es:%d",resultados);
		return main();
	}
	 else if (resultado==1)
	{
		printf("Digite el número:");
		scanf("%d",&dato);
		parImpar(dato);
		
		return main();
		
	}
	else if (resultado==3)
	{
		printf("Digite el número:");
		scanf("%d",&dato);
		int resultados= fact(dato);
		printf("El resultado es:%d\n",resultados);
		return main();
	}
	else if (resultado ==4)
	
	{
		printf("Digite el número :");
		scanf("%d",&dato);
		largoNumeroW(dato);
		
		return main();
	}
	else if (resultado==5)
	{
		
		
		
		printf("Digite el número:");
		scanf("%d",&dato);
		largoNumeroF(dato);
		return main();
		
	}
	else if (resultado==6)
	{
		printf("Digite hasta que número quiere que sea la sumatoria:");
		scanf("%d",&dato);
		int suma= sumatoria(dato);
		printf("El resultado de la sumatoria:%d\n" ,suma);
		return main();
	}
	else if (resultado==7)
	{
		printf("Digite hasta que número quiere que sea la sumatoria:");
		scanf("%d",&dato);
		int sumaW= sumawhile(dato);
		printf("El resultado de la sumatoria:%d\n" ,sumaW);
		return main();
	}
	if (resultado==8)
	{
		printf("Digite el número que desea invertir :");
		scanf("%d",&dato);
		int inv= invertir(dato);
		printf("El resultado es :%d\n",inv);
		return main();
	}
	else if (resultado==9)
	{
		printf("Digite el número:");
		scanf("%d",&dato);
		palindromo(dato);
		return main();
	}
	else
	{
		printf("Opción invalida\n");
		return main();
	}
		
		
	return 0;
}
